<?php

namespace App\Controller\Client;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MovieController extends AbstractController
{
    /**
     * @Route("/public/movie", name="public_movie")
     */
    public function index(): Response
    {
        return $this->render('public/movie/index.html.twig', [
            'controller_name' => 'MovieController',
        ]);
    }
}
