<?php

namespace App\Controller\Admin;

use App\Entity\Movie;
use App\Form\MovieType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class MovieController extends AbstractController
{
    /**
     * @Route("/admin/movie", name="admin_movie")
     */
    #[Route('/admin/movie', name: 'admin_movie')]
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Movie::class);

        return $this->render('admin/movie/index.html.twig', [
            'movies' => $repo->findAll(),
        ]);
    }

    /**
     * @Route("/admin/movie/edit/{id}", name="admin_movie_edit", requirements={ "id": "\d+" }, defaults={ "id": 0 })
     */
    #[Route('/admin/movie/edit/{id}', name: 'admin_movie_edit', requirements: ['id' => '\d+'], defaults: ['id' => 0])]
    public function edit(Request $request, $id) {
        $repo = $this->getDoctrine()->getRepository(Movie::class);
        // update / create
        $movie = $repo->find($id) ?: new Movie();
        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);
        if($form->isSubmitted()) {
            if($form->isValid()) {
                // handle files
                if($movie->getPosterFile()) {
                    if($movie->getPoster()) {// si un fichier existe déjà
                        // supprimer le fichier
                        (new Filesystem())->remove($this->getParameter('full_movies_posters_directory') . '/' . $movie->getPoster());
                    }
                    $fileName = uniqid() . $movie->getPosterFile()->guessExtension(); // create new unique filename
                    $movie->getPosterFile()->move($this->getParameter('full_movies_posters_directory'), $fileName);
                    $movie->setPoster($fileName);
                }
                if($movie->getTrailerFile()) {
                    if($movie->getTrailer()) {
                        (new Filesystem())->remove($this->getParameter('full_movies_trailers_directory') . '/' . $movie->getTrailer());
                    }
                    $fileName = uniqid() . $movie->getTrailerFile()->guessExtension(); // create new unique filename
                    $movie->getTrailerFile()->move($this->getParameter('full_movies_trailers_directory'), $fileName);
                    $movie->setTrailer($fileName);
                }

                $em = $this->getDoctrine()->getManager();
                $this->addFlash('success', 'Mofification OK');
                $em->persist($movie);
                $em->flush();
                return $this->redirectToRoute('admin_movie');
            }
            else {
                $this->addFlash('error', 'Modification impossible');
            }
        }
        return $this->render('admin/movie/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/movie/delete/{id}", name="admin_movie_delete", requirements={ "id": "\d+" }, defaults={ "id": 0 })
     */
    #[Route('/admin/movie/delete/{id}', name: 'admin_movie_delete', requirements: ['id' => '\d+'], defaults: ['id' => 0])]
    public function delete($id) {
        $repo = $this->getDoctrine()->getRepository(Movie::class);
        $movie = $repo->find($id);
        if(!$movie) {
            throw new NotFoundHttpException();
        }
        if($movie->getPoster()) {
            (new Filesystem())->remove($this->getParameter('full_movies_posters_directory') . '/' . $movie->getPoster());
        }
        if($movie->getTrailer()) {
            (new Filesystem())->remove($this->getParameter('full_movies_trailers_directory') . '/' . $movie->getTrailer());
        }
        $em = $this->getDoctrine()->getManager();
        $this->addFlash('success', 'Suppression OK');
        $em->remove($movie);
        $em->flush();
        return $this->redirectToRoute('admin_movie');
    }
}
