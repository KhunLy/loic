<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/admin/category", name="admin_category")
     */
    #[Route('/admin/category', name: 'admin_category')]
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Category::class);

        return $this->render('admin/category/index.html.twig', [
            'categories' => $repo->findAll(),
        ]);
    }

    /**
     * @Route("/admin/category/edit/{id}", name="admin_category_edit", requirements={ "id": "\d+" }, defaults={ "id": 0 })
     */
    #[Route('/admin/category/edit/{id}', name: 'admin_category_edit', requirements: ['id' => '\d+'], defaults: ['id' => 0])]
    public function edit(Request $request, $id) {
        $repo = $this->getDoctrine()->getRepository(Category::class);
        // update / create
        $category = $repo->find($id) ?: new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted()) {
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $this->addFlash('success', 'Mofification OK');
                $em->persist($category);
                $em->flush();
                return $this->redirectToRoute('admin_category');
            }
            else {
                $this->addFlash('error', 'Modification impossible');
            }
        }
        return $this->render('admin/category/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/category/delete/{id}", name="admin_category_delete", requirements={ "id": "\d+" }, defaults={ "id": 0 })
     */
    #[Route('/admin/category/delete/{id}', name: 'admin_category_delete', requirements: ['id' => '\d+'], defaults: ['id' => 0])]
    public function delete($id) {
        $repo = $this->getDoctrine()->getRepository(Category::class);
        $category = $repo->find($id);
        if(!$category) {
            throw new NotFoundHttpException();
        }
        $em = $this->getDoctrine()->getManager();
        $this->addFlash('success', 'Suppression OK');
        $em->remove($category);
        $em->flush();
        return $this->redirectToRoute('admin_category');
    }
}
