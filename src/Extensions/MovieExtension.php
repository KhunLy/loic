<?php

namespace App\Extensions;

use Symfony\Component\HttpFoundation\File\File;

Trait MovieExtension {
    /**
     * @var File | null
     */
    private $posterFile;

    /**
     * @var File | null
     */
    private $trailerFile;

    /**
     * @return File|null
     */
    public function getPosterFile(): ?File
    {
        return $this->posterFile;
    }

    /**
     * @param File|null $posterFile
     */
    public function setPosterFile(?File $posterFile)
    {
        $this->posterFile = $posterFile;
    }

    /**
     * @return File|null
     */
    public function getTrailerFile(): ?File
    {
        return $this->trailerFile;
    }

    /**
     * @param File|null $trailerFile
     */
    public function setTrailerFile(?File $trailerFile)
    {
        $this->trailerFile = $trailerFile;
    }
}