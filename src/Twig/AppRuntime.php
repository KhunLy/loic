<?php

namespace App\Twig;

use App\Entity\Category;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function poster($file)
    {
        $src = $this->container->getParameter('movies_posters_directory') . '/' . ($file ?: 'default.png');
        return sprintf('<img class="thumbnail" src="%s">', $src);
    }

    public function trailer($file)
    {
        // TODO implements
    }

    public function image($file)
    {
        // TODO implements
    }

    public function category(Category $category) {
        $style = sprintf('color:%s;border-color:%s',
            $category->getColor() ?: 'black',
            $category->getColor() ?: 'black'
        );
        return sprintf('<div class="category" style="%s">%s</div>',
            $style,
            $category->getName() ?: 'Pas de catégorie'
        );
    }
}