<?php


namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('poster', [AppRuntime::class, 'poster']),
            new TwigFilter('trailer', [AppRuntime::class, 'trailer']),
            new TwigFilter('image', [AppRuntime::class, 'image']),
            new TwigFilter('category', [AppRuntime::class, 'category']),
        ];
    }
}