<?php

namespace App\Form;

use App\Entity\Actor;
use App\Entity\Category;
use App\Entity\Movie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'required' => true
            ])
            ->add('synopsis', TextareaType::class, [
                'label' => 'Synopsis',
                'required' => false
            ])
            ->add('duration', NumberType::class, [
                'label' => 'Durée',
                'required' => true
            ])
            ->add('posterFile', FileType::class, [
                'label' => 'Poster',
                'required' => false
            ])
            ->add('trailerFile', FileType::class, [
                'label' => 'Trailer',
                'required' => false
            ])
            ->add('director', TextType::class, [
                'required' => true
            ])
            ->add('releaseYear', NumberType::class, [
                'required' => true
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                // choix de la propriété à afficher
                'choice_label' => 'name'
            ])
            ->add('actors', EntityType::class, [
                'class' => Actor::class,
                // on peut manipuler les champs de notre objet pour modifier le résultat à afficher
                'choice_label' => function(Actor $a) { return $a->getLastName() . '  ' . $a->getFirstName(); },
                'multiple' => true,
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movie::class,
        ]);
    }
}
