<?php


namespace App\Command;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends Command
{
    private $em;
    private $encoder;

    public function __construct(string $name = null, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        parent::__construct('app:create-user');
        $this->em = $manager;
        $this->encoder = $encoder;
    }

    protected function configure()
    {
        $this->addArgument(
            'email',
            InputArgument::REQUIRED,
            'Entrez un email'
        );

        $this->addArgument(
            'password',
            InputArgument::REQUIRED,
            'Entrez un password'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $raw = $input->getArgument('password');

        $user = new User();
        $user->setEmail($email);

        $encoded = $this->encoder->encodePassword($user, $raw);
        $user->setPassword($encoded);

        $user->setRoles(['ROLE_ADMIN']);

        $this->em->persist($user);
        $this->em->flush();

        return Command::SUCCESS;
    }
}